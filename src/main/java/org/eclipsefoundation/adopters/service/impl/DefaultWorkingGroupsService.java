/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.adopters.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipsefoundation.adopters.api.models.WorkingGroup;
import org.eclipsefoundation.adopters.service.WorkingGroupsService;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.LoadingCacheManager;

/**
 * Builds a list of working group definitions from an embedded list of working group definitions. This is an interim
 * solution to accelerate this project and should be replaced with a call to the foundation API to retrieve this data.
 * 
 * @author Martin Lowe
 */
@ApplicationScoped
public class DefaultWorkingGroupsService implements WorkingGroupsService {

    @Inject
    LoadingCacheManager cache;

    @Override
    public List<WorkingGroup> get() {
        return new ArrayList<>(
                cache.getList(ParameterizedCacheKey.builder().setId("all").setClazz(WorkingGroup.class).build()));
    }

    @Override
    public WorkingGroup getByName(String name) {
        return get().stream().filter(wg -> wg.getAlias().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

}
