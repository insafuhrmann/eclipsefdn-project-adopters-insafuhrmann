/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.adopters.service;

import java.util.List;

import org.eclipsefoundation.adopters.api.models.WorkingGroup;

/**
 * Defines a service that will return available working group data.
 * 
 * @author Martin Lowe
 *
 */
public interface WorkingGroupsService {

    /**
     * Returns all available working groups as a list.
     * 
     * @return list of all working group definitions from the working group API
     */
    public List<WorkingGroup> get();

    /**
     * Retrieves a single working group by its alias if available.
     * 
     * @param name the alias of the working group to retrieve
     * @return the working group with the given alias if available, or null.
     */
    public WorkingGroup getByName(String name);
}
