/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.config;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.microprofile.config.spi.Converter;

/**
 * Provides a way to bind NIO paths to MP property values.
 * 
 * @author Martin Lowe
 *
 */
public class PathConverter implements Converter<Path> {
	private static final long serialVersionUID = 1L;

	@Override
	public Path convert(String value) {
		return Paths.get(value);
	}
}